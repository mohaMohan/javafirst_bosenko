package com.javadev.marathon.bosenko;

import java.util.Arrays;

public class TaskCh12N063 {
    public static void main(String[] args) {
        int[][] array = new int[11][4];
        System.out.println("The number of students");
        System.out.println(Arrays.deepToString(completeStudentsArrayWithNumbers(array)));
        for (int i = 0; i < array.length; i++) {
            System.out.println("The average amount of students in " + (i + 1) + " classes is " + countAverageStudentsNumber(array)[i]);
        }
    }

    public static int[][] completeStudentsArrayWithNumbers(int[][] array) {
        int[][] newArr = array;
        int max = 24;
        int min = 16;
        int range = (max - min) + 1;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < 4; j++) {
                int random = (int) (Math.random() * range) + min;
                newArr[i][j] = random;
            }
        }
        return newArr;
    }

    public static int[] countAverageStudentsNumber(int[][] array) {
        int[] averageNums = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            int wholeNum = 0;
            int counter = 0;
            for (int j = 0; j < 4; j++) {
                counter++;
                wholeNum += array[i][j];
                if (counter == 4) averageNums[i] = wholeNum / counter;
            }
        }
        return averageNums;
    }
}
