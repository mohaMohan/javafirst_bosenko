package com.javadev.marathon.bosenko;

public class TaskCh10N044 {
    public static void main(String[] args) {
        System.out.println("Digital root of number is " + findDigitalRoot(0, 12345678));
    }

    public static int findDigitalRoot(int counter, int num) {
        if (num == 0) {
            if (counter < 10) {
                return counter;
            }
            num = counter;
            counter = 0;
        }
        return findDigitalRoot(counter + num % 10, num / 10);
    }
}
