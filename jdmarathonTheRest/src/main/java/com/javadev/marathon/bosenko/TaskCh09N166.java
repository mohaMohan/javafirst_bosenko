package com.javadev.marathon.bosenko;

public class TaskCh09N166 {
    public static void main(String[] args) {
        String expression = "world this wonderful Hello";
        System.out.println("Original expression: " + expression);
        System.out.println("New expression: " + swapWords(expression));
    }

    public static String swapWords(String expression) {
        String newExpression = "";
        String[] array = expression.split(" ");

        String temp = array[0];
        array[0] = array[array.length - 1];
        array[array.length - 1] = temp;

        for (int i = 0; i < array.length; i++) {
            newExpression += array[i] + " ";
        }
        return newExpression;
    }
}
