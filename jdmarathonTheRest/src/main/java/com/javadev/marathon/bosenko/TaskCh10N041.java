package com.javadev.marathon.bosenko;

public class TaskCh10N041 {
    public static void main(String[] args) {
        System.out.println("Factorial equals " + calcFactorial(5));
    }

    public static int calcFactorial(int n) {
        int result;
        if (n == 1) {
            return 1;
        }
        result = calcFactorial(n - 1) * n;
        System.out.println(result);
        return result;
    }
}
