package com.javadev.marathon.bosenko;

public class TaskCh10N047 {
    public static void main(String[] args) {
        System.out.println(findFibonacciSequenceNumber(5));
    }

    public static int findFibonacciSequenceNumber(int fib) {
        if (fib <= 1){
            return 1;
        }
        return findFibonacciSequenceNumber(fib - 1) + findFibonacciSequenceNumber(fib - 2);
    }
}
