package com.javadev.marathon.bosenko;

import java.util.Arrays;

public class TaskCh12N025 {
    public static void main(String[] args) {
        int[][] array1 = fillInArrayOne();
        System.out.println("(complete) task 'a' - " + Arrays.deepToString(array1));

        int[][] array2 = fillInArrayTwo();
        System.out.println("(complete) task 'б' - " + Arrays.deepToString(array2));

        int[][] array3 = reverseHorizontally(array1, 0);
        System.out.println("(complete) task 'в' - " + Arrays.deepToString(array3));

        int[][] array4 = reverseVertically(array2);
        System.out.println("(complete) task 'г' - " + Arrays.deepToString(array4));

        int[][] array5 = fillInArrayFive(array2);
        System.out.println("(complete) task 'д' - " + Arrays.deepToString(array5));

        int[][] array6 = fillInArraySix();
        System.out.println("(complete) task 'е' - " + Arrays.deepToString(array6));

        int[][] array7 = reverseVertically(array1);
        System.out.println("(complete) task 'ж' - " + Arrays.deepToString(array7));

        int[][] array8 = reverseHorizontally(array2, 0);
        System.out.println("(complete) task 'з' - " + Arrays.deepToString(array8));

        int[][] array9 = reverseVertically(array3);
        System.out.println("(complete) task 'и' - " + Arrays.deepToString(array9));

        int[][] array10 = reverseVertically(array8);
        System.out.println("(complete) task 'к' - " + Arrays.deepToString(array10));

        int[][] array11 = fillInArrayEleven(array3);
        System.out.println("(complete) task 'л' - " + Arrays.deepToString(array11));

        int[][] array12 = fillInArrayTwelve(array3);
        System.out.println("(complete) task 'м' - " + Arrays.deepToString(array12));

        int[][] array13 = reverseHorizontally(array6, 0);
        System.out.println("(complete) task 'н' - " + Arrays.deepToString(array13));

        int[][] array14 = fillInArrayFourteen(array6);
        System.out.println("(complete) task 'o' - " + Arrays.deepToString(array14));

        int[][] array15 = reverseVertically(array12);
        System.out.println("(complete) task 'п' - " + Arrays.deepToString(array15));

        int[][] array16 = fillInArraySixteen(array13);
        System.out.println("(complete) task 'p' - " + Arrays.deepToString(array16));

    }

    public static int[][] fillInArrayOne() {
        int[][] array = new int[4][4];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (i == 2 || j == 2) {
                    continue;
                } else if (j == 3) {
                    array[i][j] = array[i][1] * 5;
                } else {
                    array[i][j] = j + 1;
                }
            }
        }
        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (i == 2 || j == 2) {
                    continue;
                }
                array[i][j] += 10;
                if (i == 3) {
                    array[i][j] += 100;
                }
            }
        }
        return array;
    }

    public static int[][] fillInArrayTwo() {
        int n = 0;
        int[][] array = new int[4][4];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (i <= 1) {
                    if (j == 2) {
                        n += 10;
                        continue;
                    } else if (j != 3) {
                        n++;
                    }
                    array[j][i] = n;
                } else {
                    if (i == 2) {
                        if (j == 3) {
                            n *= 5;
                        }
                    } else if (i == 3 && j == 1) {
                        n -= 10;
                    } else {
                        if (j == 3) {
                            n--;
                        }
                        array[j][i] = n;
                    }
                }
            }
        }
        int[] temp = new int[array.length / 2];
        for (int i = 3; i < array.length; i++) {
            for (int j = 0; j < array.length / 2; j++) {
                temp[j] = array[j][i];
                array[j][i] = array[array.length - (j + 1)][i];
                array[array.length - (j + 1)][i] = temp[j];
            }
        }
        return array;
    }

    public static int[][] fillInArrayFive(int[][] array) {
        int[][] newArr = new int[5][4];
        int x = 0;
        for (int i = 0; i < newArr.length; i++) {
            for (int j = 0; j < newArr[0].length; j++) {
                if (i == 2) {
                    continue;
                }
                newArr[i][j] = array[j][x];
                if (j == 3) {
                    x++;
                }
            }
        }
        newArr = reverseHorizontally(newArr, 1);
        int n = newArr[1][0];
        for (int i = 0; i < array.length; i++) {
            if (i == 2) {
                n += 10;
                continue;
            } else if (i != 3) {
                n++; //переделать
            }
            newArr[2][i] = n;
        }
        return newArr;
    }

    public static int[][] fillInArraySix() {
        int[][] arr = new int[5][5];
        int n = 1;
        for (int i = 0; i < arr.length; i++) {
            if (i == 3) {
                n += 72;
                continue;
            } else if (i > 0) n += 2;
            for (int j = 0; j < arr.length; j++) {
                if (j == 0 || j == 2 || j == 3) {
                    if (j == 2) {
                        n += 10;
                    }
                    continue;
                } else if (n == 23 || n == 35) {
                    arr[j - 1][i] = 0;
                    arr[j][i] = n + 1;
                    continue;
                }
                arr[j - 1][i] = n;
                arr[j][i] = n + 1;
            }
        }
        int[] temp = new int[arr.length / 2];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length / 2; j++) {
                if (i == 1 || i == 4) {
                    temp[j] = arr[j][i];
                    arr[j][i] = arr[5 - (j + 1)][i];
                    arr[5 - (j + 1)][i] = temp[j];
                }
            }
        }
        return arr;
    }

    public static int[][] fillInArrayEleven(int[][] array) {
        int[][] newArr = new int[5][4];
        int n = 120;
        for (int i = 0; i < newArr.length - 3; i++) {
            for (int j = 0; j < newArr[0].length; j++) {
                newArr[i][j] = array[i][j];
            }
        }
        newArr = reverseVertically(newArr);
        newArr = reverseHorizontally(newArr, 4);
        for (int i = 0; i < newArr.length - 2; i++) {
            for (int j = 0; j < newArr[0].length; j++) {
                if (i == 1) {
                    n = 20;
                    //continue;
                } else if (i < 2) {
                    if (j == 2) {
                        n -= 7;
                    } else {
                        newArr[i][j] = n--;
                    }
                } else if (i == 2) {
                    if (j > 1) {
                        continue;
                    }
                    newArr[i][j] = ++n;
                }
            }
        }
        return newArr;
    }

    public static int[][] fillInArrayTwelve(int[][] array) {
        int[][] newArr = new int[5][4];
        int n = 0;
        for (int i = 0; i < newArr.length; i++) {
            if (i != 0 && i != 2) n++;
            for (int j = 0; j < newArr[i].length; j++) {
                if (i == 2) {
                    if (j == 2) {
                        continue;
                    } else if (j == 3) {
                        newArr[i][j] = array[n][0] + 10;
                    } else {
                        newArr[i][j] = array[n][0] + (j + 1);
                    }
                    // continue;
                } else {
                    newArr[i][j] = array[n][j];
                }
            }
        }
        newArr = reverseHorizontally(newArr, 1);
        return newArr;
    }

    public static int[][] fillInArrayFourteen(int[][] array) {
        int[][] newArr = reverseVertically(array);
        newArr[0][2] = 0;
        newArr[3][4] = 0;
        return newArr;
    }

    public static int[][] fillInArraySixteen(int[][] array) {
        int[][] newArr = reverseVertically(array);
        newArr[1][4] = 0;
        return newArr;
    }

    public static int[][] reverseVertically(int[][] array) {
        int[][] newArr = copyArr(array);
        int[] temp = new int[array.length / 2];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array.length / 2; j++) {
                temp[j] = newArr[j][i];
                newArr[j][i] = newArr[array.length - (j + 1)][i];
                newArr[array.length - (j + 1)][i] = temp[j];
            }
        }
        return newArr;
    }

    public static int[][] reverseHorizontally(int[][] array, int pos) {
        int[][] newArr = copyArr(array);
        int[] temp = new int[array.length / 2];

        for (int i = pos; i < array.length; i++) {
            for (int j = 0; j < array.length / 2; j++) {
                // if (i == 2) continue;
                temp[j] = newArr[i][j];
                newArr[i][j] = newArr[i][array[i].length - (j + 1)];
                newArr[i][array[i].length - (j + 1)] = temp[j];
            }
        }

        return newArr;
    }

    public static int[][] copyArr(int[][] arr) {
        int[][] newArr = new int[arr.length][arr[0].length]; // потом заменить 4ки
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) { // потом заменить 4ки
                newArr[i][j] = arr[i][j];
            }
        }
        return newArr;
    }
}
