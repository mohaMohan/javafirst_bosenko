package com.javadev.marathon.bosenko;
public class TaskCh10N042 {
    public static void main(String[] args) {
        int number = 5;
        int originalNumber = number;
        int powerOfNumber = 4;
        System.out.println(raisePower(number, originalNumber, powerOfNumber));
    }

    public static int raisePower(int number, int originalNumber, int powerOfNumber) {
        if (powerOfNumber == 1){
            return number;
        }
        return raisePower(number * originalNumber, originalNumber, powerOfNumber - 1);
    }
}
