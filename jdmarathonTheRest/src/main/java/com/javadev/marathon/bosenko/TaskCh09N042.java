package com.javadev.marathon.bosenko;

public class TaskCh09N042 {
    public static void main(String[] args) {
        String word = "Mohan";
        System.out.println(word);
        System.out.println(reverseCharacterSequence(word));
    }

    public static StringBuilder reverseCharacterSequence(String inputWord) {
        StringBuilder sb = new StringBuilder(inputWord);
        return sb.reverse();
    }
}
