package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh09N017 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter any word");
        String inputWord = scanner.nextLine();
        System.out.println("Its length is " + inputWord.length() + " symbols");

        System.out.println("The characters equality is " + areSymbolsEqual(inputWord));
    }

    public static boolean areSymbolsEqual(String inputWord) {
        char firstChar = inputWord.charAt(0);
        char lastChar = inputWord.charAt(inputWord.length() - 1);

        if (firstChar == lastChar) {
            return true;
        } else return false;
    }
}
