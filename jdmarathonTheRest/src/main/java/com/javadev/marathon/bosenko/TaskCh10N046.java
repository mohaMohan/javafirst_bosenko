package com.javadev.marathon.bosenko;

public class TaskCh10N046 {
    public static void main(String[] args) {
        int firstNumber = 10;
        int divisor = 2;
        int number = 5;
        System.out.println("n-number equals to " + calcGeometricProgressionNumber(firstNumber, divisor, number, divisor));
        System.out.println("the sum of progression numbers is " + calcNumbersSum(firstNumber, divisor, number, firstNumber));
    }

    public static int calcGeometricProgressionNumber(int progressNumber, int divisor, int lastNum, int multiplier) {
        if (lastNum - 1 == 1) {
            return progressNumber * divisor;
        }
        return calcGeometricProgressionNumber(progressNumber, divisor * multiplier, lastNum - 1, multiplier);
    }

    public static int calcNumbersSum(int progressNum, int divisor, int lastNum, int sum) {
        if (lastNum == 2) {
            return sum;
        }
        sum += progressNum * (int) Math.pow(divisor, lastNum - 1);
        return calcNumbersSum(progressNum, divisor, lastNum - 1, sum);
    }
}
