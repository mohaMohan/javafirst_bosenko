package com.javadev.marathon.bosenko;

import java.util.Arrays;

public class TaskCh12N023 {
    public static void main(String[] args) {
        int[][] array = new int[7][7];
        System.out.println("New one");
        for (int i = 0; i < array.length; i++) {
            System.out.println(Arrays.toString(fillInCrossedArray(array)[i]));
        }
        System.out.println("The other new one");
        for (int i = 0; i < array.length; i++) {
            System.out.println(Arrays.toString(fillInSnowFlakeArray(array)[i]));
        }
        System.out.println("Yet another one array");
        for (int i = 0; i < array.length; i++) {
            System.out.println(Arrays.toString(fillInHourglassArray(array)[i]));
        }
    }

    public static int[][] fillInCrossedArray(int[][] array) {
        int[][] newArray = array;
        for (int i = 0; i < array.length; i++) {
            newArray[i][i] = 1;
        }
        int counter = 0;
        for (int i = array.length - 1; i >= 0; i--) {
            newArray[i][counter] = 1;
            counter++;
        }
        return newArray;
    }

    public static int[][] fillInSnowFlakeArray(int[][] array) {
        int[][] newArray = array;
        for (int i = 0; i < array.length; i++) {
            if (i == array.length / 2) {
                for (int j = 0; j < array.length; j++) {
                    newArray[i][j] = 1;
                    newArray[j][i] = 1;
                }
            }
        }
        return newArray;
    }

    public static int[][] fillInHourglassArray(int[][] array) {
        int[][] newArray = array;
        boolean flag = true;
        int min = 0;
        int max = array.length - 1;
        for (int i = 0; i < array.length; i++) {
            if (i == array.length / 2) {
                for (int j = 0; j < array.length; j++) {
                    newArray[i][j] = 0;
                }
            }
            if (min == max || !flag) {
                flag = false;
                for (int j = 0; j < array.length; j++) {
                    if (j >= min && j <= max) {
                        newArray[i][j] = 1;
                    }
                }
                min--;
                max++;
            } else if (flag) {
                for (int j = 0; j < array.length; j++) {
                    if (j >= min && j <= max) {
                        newArray[i][j] = 1;
                    }
                }
                min++;
                max--;
            }
        }
        return newArray;
    }
}