package com.javadev.marathon.bosenko;

public class TaskCh09N107 {
    public static void main(String[] args) {
        String word = "maarrooon";
        if (word.equalsIgnoreCase(swapLetters(word))) {
            System.out.println("Invalid word");
        } else System.out.println("New word is " + swapLetters(word));
    }

    public static String swapLetters(String word) {
        String newWord = "";
        int indexOfA, indexOfO;

        if (word.indexOf("a") >= 0 && word.lastIndexOf("o") >= 0) { // because it returns -1 if there's no 'a'
            indexOfA = word.indexOf("a");
            indexOfO = word.lastIndexOf("o");
            char[] letterArray = word.toCharArray();
            letterArray[indexOfA] = 'o';
            letterArray[indexOfO] = 'a';
            for (int i = 0; i < letterArray.length; i++) {
                newWord += letterArray[i];
            }
            return newWord;
        }
        return word;
    }
}


