package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh09N015 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter any word");
        String inputWord = scanner.nextLine();
        System.out.println("Its length is " + inputWord.length() + " symbols");
        System.out.println("Your symbol is " + outputSymbol(inputWord));
    }

    public static char outputSymbol(String word) {
        System.out.println("Enter a number within a symbol range");
        int nSymbol = scanner.nextInt() - 1;
        return word.charAt(nSymbol);
    }
}
