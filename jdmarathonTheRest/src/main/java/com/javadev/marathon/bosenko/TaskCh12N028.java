package com.javadev.marathon.bosenko;

import java.util.Arrays;

public class TaskCh12N028 {
    public static void main(String[] args) {
        int[][] array = new int[7][7];
        System.out.println("New one");
        for (int i = 0; i < array.length; i++) {
            System.out.println(Arrays.toString(fillInShellArray(array)[i]));
        }
    }

    public static int[][] fillInShellArray(int[][] array) {
        int[][] newArr = array;
        int n = 1;
        int minBreaker = 0;
        int maxBreaker = array.length;
        int counter = 0;
        while (counter != array.length / 2) {
            for (int i = minBreaker; i < maxBreaker; i++) {
                for (int j = minBreaker; j < maxBreaker; j++) {
                    if (i != minBreaker && j != maxBreaker - 1) continue;
                    newArr[i][j] = n++;
                }
            }
            for (int i = maxBreaker - 1; i > minBreaker; i--) {
                for (int j = maxBreaker - 2; j >= minBreaker; j--) {
                    if (i != maxBreaker - 1 && j != minBreaker) continue;
                    newArr[i][j] = n++;
                }
            }
            counter++;
            minBreaker++;
            maxBreaker--;
        }
        newArr[array.length / 2][array.length / 2] = array.length * array.length;
        return newArr;
    }
}
