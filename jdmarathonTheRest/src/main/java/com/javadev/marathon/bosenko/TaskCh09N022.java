package com.javadev.marathon.bosenko;

public class TaskCh09N022 {
    public static void main(String[] args) {
        String evenNumberWord = "SnowLeopardSnowLeopard";
        System.out.println("Half is " + splitInTwo(evenNumberWord));
    }

    public static String splitInTwo(String evenNumberWord) {
        String newWord = evenNumberWord;
        return newWord.substring(0, evenNumberWord.length() / 2);
    }
}
