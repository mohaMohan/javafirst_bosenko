package com.javadev.marathon.bosenko;

public class TaskCh10N043 {
    public static void main(String[] args) {
        System.out.println("The sum of the first number digits equals to " + sumDigits(123, 0));
        System.out.println("Amount of the second number digits equals to " + calcAmountOfNumbers(456892, 0));
    }

    public static int sumDigits(int num, int newNum) {
        newNum += num % 10;
        num /= 10;
        if (num == 0) {
            return newNum;
        }
        return sumDigits(num, newNum);
    }

    public static int calcAmountOfNumbers(int num, int counter) {
        if (num % 10 == 0) {
            return counter;
        }
        counter++;
        return calcAmountOfNumbers(num / 10, counter);
    }
}
