package com.javadev.marathon.bosenko;

import java.util.Arrays;

public class TaskCh12N024 {
    public static void main(String[] args) {
        int[][] array = new int[6][6];
        System.out.println("1st one");
        for (int i = 0; i < array.length; i++) {
            System.out.println(Arrays.toString(fillInFirstArray(array)[i]));
        }
        array = new int[6][6];
        System.out.println("2nd one");
        for (int i = 0; i < array.length; i++) {
            System.out.println(Arrays.toString(fillInSecondArray(array)[i]));
        }
    }

    public static int[][] fillInFirstArray(int[][] array) {
        int[][] newArr = array;
        for (int i = 0; i < array.length; i++) {
            newArr[0][i] = 1;
            newArr[i][0] = 1;
        }
        for (int i = 1; i < array.length; i++) {
            for (int j = 1; j < array.length; j++) {
                newArr[i][j] = newArr[i][j - 1] + newArr[i - 1][j];
            }
        }
        return newArr;
    }

    public static int[][] fillInSecondArray(int[][] array) {
        int[][] newArr = array;
        for (int i = 0; i < array.length; i++) {
            newArr[0][i] = i + 1;
            newArr[i][0] = i + 1;
        }
        for (int i = 1; i < array.length; i++) {
            for (int j = 1; j < array.length; j++) {
                if (newArr[i][j - 1] == array.length && newArr[i - 1][j] == array.length)
                    newArr[i][j] = 1;
                else if (newArr[i][j - 1] == newArr[i - 1][j])
                    newArr[i][j] = newArr[i][j - 1] + 1;
            }
        }
        return newArr;
    }
}