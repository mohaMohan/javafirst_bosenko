package com.javadev.marathon.bosenko;

public class TaskCh10N045 {
    public static void main(String[] args) {
        int firstNumber = 10;
        int progressionDiff = 3;
        int lastNumber = 5;
        System.out.println("n-number equals to " + calcProgressionNumber(firstNumber, progressionDiff, lastNumber));
        System.out.println("the sum of progression numbers is " + calcNumbersSum(firstNumber, progressionDiff, lastNumber, 0));
    }

    public static int calcProgressionNumber(int progressNumber, int diff, int lastNumber) {
        if (lastNumber == 1) {
            return progressNumber;
        }
        return calcProgressionNumber(progressNumber + diff, diff, lastNumber - 1);
    }

    public static int calcNumbersSum(int firstNumber, int diff, int lastNumber, int sum) {
        if (lastNumber > 0) {
            sum += firstNumber;
        }
        if (lastNumber == 1) {
            return sum;
        }
        return calcNumbersSum(firstNumber + diff, diff, lastNumber - 1, sum);
    }
}
