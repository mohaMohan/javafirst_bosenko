package com.javadev.marathon.bosenko;

public class TaskCh10N048 {
    public static void main(String[] args) {
        int[] array = {4, 1, 45, 34, 15, 6, 12};
        System.out.println("Max array element is " + findMaxArrayElement(array, 0, Integer.MIN_VALUE));
    }

    public static int findMaxArrayElement(int[] array, int counter, int maxElement) {
        if (maxElement < array[counter]) {
            maxElement = array[counter];
        } else if (counter == array.length - 1) {
            return maxElement;
        }
        return findMaxArrayElement(array, counter + 1, maxElement);
    }
}
