package com.javadev.marathon.bosenko;

public class TaskCh02N013 {
    public static void main(String[] args) {
        int num = 210;
        int newNum = 0;
        int lastDigit;
        System.out.println("Number is " + num);
        
        while (num != 0) {
            lastDigit = num % 10;
            num /= 10;
            newNum += lastDigit;
            if (num != 0) {
                newNum *= 10;
            }
        }
        System.out.println("Reversed number is " + newNum);
    }
}
