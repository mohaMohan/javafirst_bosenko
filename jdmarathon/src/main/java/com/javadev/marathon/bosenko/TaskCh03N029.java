package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh03N029 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        int z = scanner.nextInt();

        System.out.println(isBothNumbersOdd(x, y));
        System.out.println(isNumberLessThen20(x, y));
        System.out.println(isOneEqualsToZero(x, y));
        System.out.println(isEveryNumberNegative(x, y, z));
        System.out.println(isDividedBy5(x, y, z));
        System.out.println(isOneHigherThen100(x, y, z));
        scanner.close();
    }

    public static boolean isBothNumbersOdd(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    public static boolean isNumberLessThen20(int x, int y) {
        return (x < 20 && y > 20) || (x > 20 && y < 20);
    }

    public static boolean isOneEqualsToZero(int x, int y) {
        return x == 0 || y == 0;
    }

    public static boolean isEveryNumberNegative(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    public static boolean isDividedBy5(int x, int y, int z) {
        x %= 5;
        y %= 5;
        z %= 5;
        return (y != 0 && z != 0) || (x != 0 && y != 0) || (x != 0 && z != 0);
    }

    public static boolean isOneHigherThen100(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}
