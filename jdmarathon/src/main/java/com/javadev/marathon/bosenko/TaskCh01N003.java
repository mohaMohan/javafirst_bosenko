package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh01N003 {
    public static void main(String[] args) {
        System.out.println("Your number is " + printOutNumber());
    }

    public static int printOutNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("You input a number");
        int num = scanner.nextInt();
        return num;
    }
}
