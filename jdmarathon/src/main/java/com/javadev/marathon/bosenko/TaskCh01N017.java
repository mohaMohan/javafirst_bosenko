package com.javadev.marathon.bosenko;


public class TaskCh01N017 {

    public static double formula1(double x) {
        return Math.sqrt(1 - Math.pow(Math.sin(x), 2));
    }

    public static double formula2(double a, double b, double c, double x) {
        return 1 / Math.sqrt(Math.pow(a * x, 2) + b * x + c);
    }

    public static double formula3(double x) {
        return Math.sqrt(x + 1) + Math.sqrt(x - 1) / 2 * Math.sqrt(x);
    }

    public static double formula4(double x) {
        return Math.abs(x) + Math.abs(x + 1);
    }
}
