package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh02N031 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        System.out.println("the x is " + toReverse(n));
    }

    public static int toReverse(int n) {
        int newNum = n;
        if (newNum < 100 || newNum > 999) {
            return -1;
        } else {
            int lastDigit = 0;

            while (newNum > 10) {
                lastDigit += newNum % 10;//separate the last digit
                newNum /= 10; //cut down the main number every time its greater then 10
                if (newNum > 10) {
                    lastDigit *= 10;//stop working when there's no need to increase the reversed part of the number
                }
            }
            return (newNum * 100) + lastDigit;
        }
    }
}
