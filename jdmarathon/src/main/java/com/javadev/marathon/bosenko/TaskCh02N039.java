package com.javadev.marathon.bosenko;

public class TaskCh02N039 {
    public static final int ANGLE_DEGREES = 30;

    public static void main(String[] args) {
        System.out.println("angle is " + calculateAngleDegree(1, 30, 30));
    }

    public static double calculateAngleDegree(int h, int m, int s) {
        int calculatedHours = h % 12;

        if ((calculatedHours > 0 && calculatedHours <= 23) || (m > 0 && m <= 59) || (s > 0 && s <= 59)) {
            return ANGLE_DEGREES * calculatedHours + m * 0.5 + s * (1.0 / 120);
        } else return -1;
    }
}
