package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh02N043 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Input two numbers");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println("The result is " + divideChecker(a, b));
    }

    public static int divideChecker(int a, int b) {
        return (a % b == 0 && b % a == 0) ? 1 : -1;
    }
}


