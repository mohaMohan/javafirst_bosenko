package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh05N010 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the current exchange rate for dollar");
        int exchangeRate = scanner.nextInt();
        convertDollars(exchangeRate);
    }

    public static void convertDollars(int exchangeRate) {
        int[] dollarArray = new int[20];

        for (int i = 0; i < 20; i++) {
            dollarArray[i] = (i + 1) * exchangeRate; /*setting the value for each array element
                                                     and converting it into dollar at the same time*/
            System.out.println((i + 1) + "$ equals to " + dollarArray[i] + " rub");
        }
    }
}
