package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh04N067 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int date = scanner.nextInt();
        System.out.println("The day number " + date + " is a " + defineDay(date));
    }

    public static String defineDay(int date) {
        if (date % 7 == 0 || date % 7 == 6) {
            return "WEEKEND";
        } else return "WORKDAY";
    }
}
