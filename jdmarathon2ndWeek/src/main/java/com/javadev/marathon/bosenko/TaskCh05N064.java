package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh05N064 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a number of residents and amount of land:");
        int peopleQuantity = scanner.nextInt();
        int area = scanner.nextInt();
        System.out.println("The averageDensity equals to " + calculatePopulationDensity(peopleQuantity, area));
    }

    public static int calculatePopulationDensity(int peopleQuantity, int area) {
        if (peopleQuantity <= 0 || area <= 0) {
            return -1;
        } else {
            int averageDensity = peopleQuantity / (area * 12);
            return averageDensity;
        }
    }
}
