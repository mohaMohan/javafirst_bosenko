package com.javadev.marathon.bosenko;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh06N008 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter any number starting with 1: ");
        int num = scanner.nextInt();
        System.out.println(Arrays.toString(calculateNumbersForLimitedSequence(num)));
    }

    public static int[] calculateNumbersForLimitedSequence(int num) {
        int arrayLength = 1;
        while (Math.pow(arrayLength, 2) < num) {
            arrayLength++;
        }
        int[] array = new int[arrayLength - 1];
        for (int i = 0; i < array.length; i++) {
            array[i] = (i + 1) * (i + 1);
        }
        return array;
    }
}
