package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh04N036 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int min = scanner.nextInt();
        System.out.println("At " + min + " minutes the light is " + defineColor(min));
    }

    public static String defineColor(int min) {
        if (min % 5 == 0 || min % 5 > 3) {
            return "red";
        } else return "green";
    }
}
