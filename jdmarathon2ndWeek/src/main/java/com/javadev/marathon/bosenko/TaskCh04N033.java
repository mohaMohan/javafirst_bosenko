package com.javadev.marathon.bosenko;

public class TaskCh04N033 {
    public static void main(String[] args) {
        int num = 123;
        int lastDigit = num % 10;
        System.out.println("it's " + isEvenNum(lastDigit) + " that the last digit of the number is even");
        System.out.println("it's " + isOddNum(lastDigit) + " that the last digit of the number is odd");

    }

    public static boolean isEvenNum(int lastDigit) {
        if (lastDigit % 2 == 0) {
            return true;
        } else return false;
    }

    public static boolean isOddNum(int lastDigit) {
        if (lastDigit % 2 == 0) {
            return false;
        } else return true;
    }
}
