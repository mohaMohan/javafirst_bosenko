package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh04N115 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int theYear = scanner.nextInt();
        findYearSymbol(theYear);
    }

    public static void findYearSymbol(int inputYear) {
        String[] symbolArr = {"Rat", "Cow", "Tiger", "Hare", "Dragon", "Snake", "Horse", "Sheep", "Monkey", "Rooster", "Dog", "Pig"};
        String[] symbolColor = {"green", "red", "yellow", "white", "black"};
        String yearAnimal, yearColor;

        int symbolNumber = (inputYear - 1984) % 12;
        if (symbolNumber < 0) symbolNumber += 12;

        yearAnimal = symbolArr[symbolNumber];

        if (inputYear % 2 == 0) {
            symbolNumber = (inputYear - 1984) % 5;
            if (symbolNumber < 0) symbolNumber += 5;
        } else {
            symbolNumber = ((inputYear - 1) - 1984) % 5;
            if (symbolNumber < 0) symbolNumber += 5;
        }
        yearColor = symbolColor[symbolNumber];
        System.out.println("This's a year of " + yearColor + " " + yearAnimal);
    }
}

