package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh04N106 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a current month number:");
        int month = scanner.nextInt();
        System.out.println(defineSeason(month) + " is now");
    }

    public static String defineSeason(int month) {
        switch (month) {
            case 12:
            case 1:
            case 2:
                return "Winter";
            case 3:
            case 4:
            case 5:
                return "Spring";
            case 6:
            case 7:
            case 8:
                return "Summer";
            case 9:
            case 10:
            case 11:
                return "Autumn";
            default:
                return "The number is out of range";
        }
    }
}
