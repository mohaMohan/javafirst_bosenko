package com.javadev.marathon.bosenko;

public class TaskCh05N038 {
    public static void main(String[] args) {
        int wholeDistance = 1000;
        System.out.println("The Husband is " + findExactPosition(wholeDistance) + " m from home");
        System.out.println("He walked about " + calculateTotalDistance(wholeDistance) + " metres");

    }

    public static int findExactPosition(int wholeDistance) {
        int distanceFromHome = wholeDistance;
        for (int i = 2; i <= 100; i++) {
            distanceFromHome -= 1000 / i;
            i++;
            distanceFromHome += 1000 / i;
        }
        return distanceFromHome;
    }

    public static int calculateTotalDistance(int wholeDistance) {
        int backAndForthDistance = wholeDistance;
        for (int i = 2; i <= 100; i++) {
            int num = 1000 / i;
            backAndForthDistance += num;
        }
        return backAndForthDistance;
    }
}
