package com.javadev.marathon.bosenko;

import java.util.Scanner;

public class TaskCh04N015 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter the present year and this month number: ");
        int thisYear = scanner.nextInt();
        int thisMonth = scanner.nextInt();
        System.out.println("Now enter your year of birth and a birth-month number: ");
        int birthYear = scanner.nextInt();
        int birthMonth = scanner.nextInt();

        System.out.println("You're " + calculateAge(thisYear, thisMonth, birthYear, birthMonth) + " years old");
    }

    public static int calculateAge(int thisYr, int thisMth, int birthYr, int birthMth) {
        int age = thisYr - birthYr;
        if (thisMth < birthMth) { /*if this month is less than a month of birth it's not reached
                                    so we need to reduce the age number*/
            return age - 1;
        } else return age; // if not, we'll just return the age parameter
    }
}
